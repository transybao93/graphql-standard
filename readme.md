## Giới thiệu cấu trúc GraphQL tham khảo
src/graphql
src/graphql/mutations
src/graphql/queries
src/graphql/types

## Dataloader
Dùng để batch và caching request

## Query tham khảo
```javascript
    query {
        user (id: "abc"){
            id,
            name
        }
        organization(id: "123"){
            id,
            name,
            phone,
            users{
            name
            }
        }
    }
```
## Contribute
Mọi sự đóng góp vui lòng tạo PR (pull request) nhé

## Donate
Buy me a coffee...