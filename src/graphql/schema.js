import {
  GraphQLObjectType,
  GraphQLSchema,
} from 'graphql';
import {
  user,
  organization
} from './queries';
import {
  register
} from './mutations';

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    user: user,
    organization: organization
  }
});

const RootMutation = new GraphQLObjectType({
  name: 'RootMutation',
  description: 'Mutations',
  fields: () => ({
    //- adding more mutations
    register: register,
  }),
});

export default new GraphQLSchema({
  query: RootQuery,
  mutation: RootMutation,
});;
