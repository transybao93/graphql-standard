
const {
    UserType,
    OrganizationType
} = require('./types');
import {
    GraphQLID,
} from 'graphql';
import _ from 'lodash';

const db = {
	users: [
		{
			organization: '123', // this is a relation by id
			id: 'abc',
			name: 'Elon Musk',
			password: 'password123',
    },
    {
			organization: '123', // this is a relation by id
			id: '111',
			name: 'baobao',
			password: '0000',
    },
    {
			organization: '11', // this is a relation by id
			id: '222',
			name: 'haha',
			password: '123123',
		},
	],
	organizations: [
		{
			users: ['abc'], // this is a relation by ids
			id: '123',
			name: 'Space X',
			phone: 1217467928,
    },
    {
			users: ['222'], // this is a relation by ids
			id: '11',
			name: 'Space X',
			phone: 1217467928,
		}
	],
};
//- queries
const user = {
    type: UserType,
    args: {
        id: {type: GraphQLID}
    },
	resolve(parent, args){
        console.log('argument', args);
        return _.find(db.users, {id: args.id});
    }
};

const organization = {
    type: OrganizationType,
    args: {
      id: {type: GraphQLID}
    },
    resolve(parent, args) {
      return _.find(db.organizations, {id: args.id});
    }
};

export {
	user,
	organization
};