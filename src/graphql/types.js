import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
    GraphQLList
} from 'graphql';
import _ from 'lodash';

const db = {
	users: [
		{
			organization: '123', // this is a relation by id
			id: 'abc',
			name: 'Elon Musk',
			password: 'password123',
    },
    {
			organization: '123', // this is a relation by id
			id: '111',
			name: 'baobao',
			password: '0000',
    },
    {
			organization: '11', // this is a relation by id
			id: '222',
			name: 'haha',
			password: '123123',
		},
	],
	organizations: [
		{
			users: ['abc'], // this is a relation by ids
			id: '123',
			name: 'Space X',
			phone: 1217467928,
    },
    {
			users: ['222'], // this is a relation by ids
			id: '11',
			name: 'Space X',
			phone: 1217467928,
		}
	],
};
export const UserType = new GraphQLObjectType({
    name: "User",
    fields: () => ({
        id: {
            type: GraphQLID
        },
            name: {
            type: GraphQLString
        },
            password: {
            type: GraphQLString
        }
    })
});
  
export const OrganizationType = new GraphQLObjectType({
    name: "Organization",
    fields: () => ({
        id: {type: GraphQLID},
        name: {type: GraphQLString},
        phone: {type: GraphQLInt},
        users: {
            type: GraphQLList(UserType),
            resolve(parent, args){
                return _.filter(db.users, {organization: parent.id});
            }
        }
    })
    });