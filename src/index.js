import express from 'express';
import graphQLServer from 'express-graphql';
// const schema = require('./graphql/schema');
import schema from './graphql/schema';

var app = express();
app.use('/graphql', graphQLServer({
	schema,
	graphiql: true
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));